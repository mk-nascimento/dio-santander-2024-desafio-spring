package edu.dio.patterns.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.dio.patterns.model.Address;
import edu.dio.patterns.model.Client;
import edu.dio.patterns.repository.AddressRepository;
import edu.dio.patterns.repository.ClientRepository;
import edu.dio.patterns.service.ClientService;
import edu.dio.patterns.service.ViaCepService;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired // NOSONAR
    private ClientRepository repository;
    @Autowired // NOSONAR
    private AddressRepository addrRepository;
    @Autowired // NOSONAR
    private ViaCepService viaCep;

    @Override
    public Client save(Client client) {
        return saveWithZipCode(client);
    }

    @Override
    public List<Client> read() {
        return repository.findAll();
    }

    @Override
    public Optional<Client> readUnique(Long id) {
        return repository.findById(id);
    }

    @Override
    public Client update(Long id, Client client) {
        Optional<Client> entity = repository.findById(id);
        if (entity.isPresent())
            return saveWithZipCode(entity.get());
        return client;
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    private Client saveWithZipCode(Client client) {
        String zipCode = client.getAddress().getCep();
        Address address = addrRepository.findById(zipCode).orElseGet(() -> {
            Address instance = viaCep.consultarCep(zipCode);
            return addrRepository.save(instance);
        });
        client.setAddress(address);
        return repository.save(client);
    }
}
