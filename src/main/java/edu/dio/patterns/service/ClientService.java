package edu.dio.patterns.service;

import java.util.List;
import java.util.Optional;

import edu.dio.patterns.model.Client;

public interface ClientService {
    Client save(Client client);

    List<Client> read();

    Optional<Client> readUnique(Long id);

    Client update(Long id, Client client);

    void delete(Long id);
}
