package edu.dio.patterns.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity(name = "_address")
public class Address {
    @Id
    @Column(name = "zip_code", nullable = false)
    private String cep;

    @Column(name = "street")
    private String logradouro;
    @Column(name = "additional_info")
    private String complemento;
    @Column(name = "neighborhood")
    private String bairro;
    @Column(name = "city")
    private String localidade;
    @Column(name = "state")
    private String uf;
    private String ibge;
    private String gia;
    @Column(name = "area_code")
    private String ddd;
    private String siafi;
}
