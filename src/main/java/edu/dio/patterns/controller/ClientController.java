package edu.dio.patterns.controller;

import java.util.List;
import java.util.Optional;

import javax.naming.NameNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import edu.dio.patterns.model.Client;
import edu.dio.patterns.service.ClientService;

@RestController
@RequestMapping("client")
public class ClientController {
    @Autowired // NOSONAR
    private ClientService service;

    @PostMapping
    public ResponseEntity<Client> save(@RequestBody Client entity) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(entity));
    }

    @GetMapping
    public ResponseEntity<List<Client>> read() {
        return ResponseEntity.ok(service.read());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Client> readUnique(@PathVariable Long id, @RequestBody Client entity)
            throws NameNotFoundException {
        Optional<Client> instance = service.readUnique(id);
        if (Boolean.FALSE.equals(instance.isPresent()))
            throw new NameNotFoundException();

        return ResponseEntity.ok(instance.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Client> update(@PathVariable Long id, @RequestBody Client cliente) {
        service.update(id, cliente);
        return ResponseEntity.ok(cliente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
