package edu.dio.patterns.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.dio.patterns.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
}
