package edu.dio.patterns.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.dio.patterns.model.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, String> {
}
