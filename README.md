# Explorando Padrões de Projetos na Prática com Java

Este projeto é uma aplicação de exemplo que demonstra a implementação de padrões de projetos utilizando Java com
Spring Boot e Maven. O foco do projeto é uma REST API que gerencia um relacionamento N:1 entre `User` e `Address`.

## Tecnologias Utilizadas

- Java 21
- Spring Boot 3.2.5
- Maven
- H2 Database
- Spring Data JPA (Java Persistence API)
- Lombok
- DevContainer (Ambiente de desenvolvimento no VS Code)

## Funcionalidades

- CRUD de Clientes (`Client`)
- Relacionamento N:1 entre Clientes e Endereços

## Instalação e Execução

### Pré-requisitos

- Java 21+
- Maven

### Passos para Execução

1. Clone o repositório:
   ```sh
   git clone https://gitlab.com/mk-nascimento/dio-santander-2024-desafio-spring.git
   cd dio-santander-2024-desafio-spring
   ```

2. Compile o projeto:
   ```sh
   mvn clean install
   ```

3. Execute a aplicação:
   ```sh
   mvn spring-boot:run
   ```

A aplicação estará disponível em `http://localhost:8080`.

## Endpoints da API

### Clientes (`Client`)

- **POST /client**: Cria um novo cliente
- **GET /client**: Lista todos os clientes
- **GET /client/{id}**: Obtém um cliente específico por ID
- **PUT /client/{id}**: Atualiza um cliente existente
- **DELETE /client/{id}**: Deleta um cliente específico

## Referências

- [Spring Boot Documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
- [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#using.devtools)
- [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
- [Spring Web](https://docs.spring.io/spring-boot/docs/3.2.5/reference/htmlsingle/index.html#web)
- [Maven Documentation](https://maven.apache.org/guides/index.html)
- [H2 Database Documentation](http://www.h2database.com/html/main.html)
